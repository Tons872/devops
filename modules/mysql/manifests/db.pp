define mysql::db($schema, $user = $title, $password){
    Class['mysql::server'] -> Mysql::Db[$title]
    exec {"$title-user":
        unless => "mysql -u$user -p$password $schema",    
        command => "mysql -uroot -e \" GRANT ALL PRIVILEGES ON \
                                    store_schema.* to '$user'@'%' \
                                    IDENTIFIED BY '$password'; \"",
        path => "/usr/bin/",
        require => Exec["$title-schema"]
    }   
  exec {"$title-schema":
      unless => "mysql -uroot $schema",
      command => "mysqladmin -uroot create $schema",
      path => "/usr/bin/",
  }
}

