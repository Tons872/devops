include mysql::client
package {["vim", "tomcat7"]:
	ensure => installed,
	require => Exec["apt-update"]
}
file {"/var/lib/tomcat7/conf/.keystore":
	owner => root,
	group => tomcat7,
	mode => 0640,
	source => "/vagrant/manifests/.keystore",
	require => Package["tomcat7"],
	notify => Service["tomcat7"],
}
file {"/var/lib/tomcat7/conf/server.xml":
	owner => root,
	group => tomcat7,
	mode => 0644,
	source => "puppet:///modules/tomcat/server.xml",
	require => Package["tomcat7"],
	notify => Service["tomcat7"],
}
file {"/var/lib/tomcat7/conf/context.xml":
	owner => root,
	group => tomcat7,
	mode => 0644,
	content => template("tomcat/context.xml"),
	require => Package["tomcat7"],
	notify => Service["tomcat7"],
}
service {"tomcat7":
	ensure => running,
	enable => true,
	hasrestart => true,
	hasstatus => true,
	require => Package["tomcat7"],
}
