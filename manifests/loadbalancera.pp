#exec {"apt-update":
#	command => "/usr/bin/apt-get update",
#}
package {"haproxy":
	ensure => installed,
	#require => Exec["apt-update"],
}
service {"haproxy":
	ensure => running,
	enable => true,
	hasstatus => true,
	hasrestart => true,
	require => Package["haproxy"],
}
file {"/etc/haproxy/haproxy.cfg":
	owner => "root",
	group => "root",
	mode  => 0644,
	content => template("/vagrant/manifests/haproxy.cfg"),
	require => Package["haproxy"],
	notify => Service["haproxy"]
}
package {"keepalived":
	ensure => installed,
	require => Package["haproxy"]
}
file {"/etc/keepalived/keepalived.conf":
	owner => 'root',
	group => 'root',
	mode =>	0640,
	content => template("/vagrant/manifests/keepalived.conf")
}
file {"/etc/keepalived/master.sh":
	owner => 'root',
        group => 'root',
        mode => 0640,
        content => template("/vagrant/manifests/master.sh")
}
